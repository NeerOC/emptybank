package tasks;

import core.Brain;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.task.Task;

public class doBank extends Task {

    @Override
    public boolean validate() {
        if(!Game.isLoggedIn()){
            return false;
        }
        if(Brain.exChangePos.distance() > 10){
            return false;
        }
        if(!Brain.GotBankStats) {
            return true;
        }
        return Brain.RealinvCount == 0 && Brain.bankCount > 0;
    }

    @Override
    public int execute() {
        openBank();
        getStats();
        getItems();
        return 75;
    }

    private void getItems(){
        if(Bank.isOpen()){
            Bank.setWithdrawMode(Bank.WithdrawMode.NOTE);
            if(Bank.getWithdrawMode() == Bank.WithdrawMode.NOTE) {
                Item[] win = Bank.getItems(Item::isExchangeable);
                for (Item iWin : win) {
                    if (iWin.isExchangeable() && Inventory.getCount() < 28) {
                        Bank.withdrawAll(iWin.getId());
                        Time.sleep(100,300);
                    }
                }
            }
        }
    }

    private void getStats(){
        if(Bank.isOpen()){
            if(!Brain.GotBankStats && Inventory.getCount() > 0){
                Bank.depositInventory();
                Time.sleepUntil(Inventory::isEmpty, Random.low(1000, 2000));
            }
            Brain.bankCount = Bank.getItems(item -> item.isExchangeable() && item.getId() != 995).length;
            Brain.GotBankStats = true;
        }
    }

    private void openBank(){
        if(!Bank.isOpen()){
            Npcs.getNearest(a->a.containsAction("Bank")).interact("Bank");
            Time.sleepUntil(Bank::isOpen, Random.low(1000, 2000));
        }
    }
}
