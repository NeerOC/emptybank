package tasks;

import core.Brain;
import org.rspeer.runetek.api.Game;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Quit extends Task {
    @Override
    public boolean validate() {
        if(!Game.isLoggedIn()){
            return false;
        }
        return Brain.GotBankStats && Brain.RealinvCount == 0 && Brain.bankCount == 0 && Exchange.getFinished() == 0 ;
    }

    @Override
    public int execute() {
        Log.info("All done, have fun!");
        return -1;
    }
}
