package tasks;

import core.Brain;
import org.rspeer.runetek.api.Game;
import org.rspeer.script.task.Task;
import util.Walking;

public class Walk extends Task {

    @Override
    public boolean validate() {
        if(!Game.isLoggedIn()){
            return false;
        }
        return Brain.exChangePos.distance() > 10;
    }

    @Override
    public int execute() {
        Walking.walkTo(Brain.exChangePos,5,true);
        return 150;
    }
}
