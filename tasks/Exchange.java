package tasks;

import core.Brain;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.EnterInput;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.GrandExchangeSetup;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.providers.RSGrandExchangeOffer;
import org.rspeer.script.task.Task;

public class Exchange extends Task {

    @Override
    public boolean validate() {
        if(!Game.isLoggedIn()){
            return false;
        }
        if(Brain.RealinvCount > 0){
            return false;
        }
        if(getFinished() > 0){
            return true;
        }
        if(Brain.exChangePos.distance() > 10){
            return false;
        }
        return false;
    }

    @Override
    public int execute() {
        openExchange();
        getStats();
        getAll();
        sellAll();
        return 200;
    }

    private void getStats(){
        if(GrandExchange.isOpen()){
        if(Brain.isMember){
            Brain.EmptyOffers = getOfferCount();
        }else {
            Brain.EmptyOffers = getOfferCount() - 5;
        }
        }
    }

    private int getOfferCount(){
        return GrandExchange.getOffers(x->x.getProgress() == RSGrandExchangeOffer.Progress.EMPTY).length;
    }

    public static int getFinished(){
        return GrandExchange.getOffers(O->O.getProgress() == RSGrandExchangeOffer.Progress.FINISHED).length;
    }

    private void getAll(){
        if(getFinished() > 0){
            if(GrandExchange.collectAll()){
                Time.sleep(600,800);
            }
        }
    }

    private void sellAll(){
        if(GrandExchange.isOpen()) {
            if (getOfferCount() > 0) {
                if (GrandExchange.getView() == GrandExchange.View.OVERVIEW) {
                    GrandExchange.createOffer(RSGrandExchangeOffer.Type.SELL);
                    Time.sleepUntil(() -> GrandExchange.getView() == GrandExchange.View.SELL_OFFER, Random.low(1800, 2000));
                }

                if (GrandExchange.getView() == GrandExchange.View.SELL_OFFER && GrandExchangeSetup.getItem() == null) {
                    Item antiNull = Inventory.getFirst(Item::isExchangeable);
                    if(antiNull != null){
                        GrandExchangeSetup.setItem(Inventory.getFirst(Item::isExchangeable).getId());
                        Time.sleepUntil(() -> GrandExchangeSetup.getItem() != null, Random.low(1800, 2000));
                    }
                }

                if (GrandExchange.getView() == GrandExchange.View.SELL_OFFER && GrandExchangeSetup.getItem() != null) {
                    if (!EnterInput.isOpen() && GrandExchangeSetup.getPricePerItem() != 1) {
                        Interfaces.getComponent(465, 24, 12).click();
                        Time.sleepUntil(EnterInput::isOpen, Random.low(1800, 2000));
                    }
                }
                if (EnterInput.isOpen() && GrandExchangeSetup.getPricePerItem() != 1) {
                    EnterInput.initiate(1);
                    Time.sleepUntil(() -> GrandExchangeSetup.getPricePerItem() == 1, Random.low(1800, 2000));
                }

                if (GrandExchange.getView().equals(GrandExchange.View.SELL_OFFER) && GrandExchangeSetup.getPricePerItem() == 1) {
                    GrandExchangeSetup.confirm();
                    Time.sleepUntil(() -> GrandExchange.getView() == GrandExchange.View.OVERVIEW, Random.low(1800, 2000));
                }
            }
        }
        }

    private void openExchange(){
        if(!GrandExchange.isOpen()){
            Npcs.getNearest(a -> a.containsAction("Exchange")).interact("Exchange");
            Time.sleepUntil(GrandExchange::isOpen, 800);
        }
    }
}
