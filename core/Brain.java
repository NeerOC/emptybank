package core;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;
import tasks.Exchange;
import tasks.Quit;
import tasks.Walk;
import tasks.doBank;

import java.awt.*;


@ScriptMeta(name = "Empty Bank", desc = "Sells all your stuff.", developer = "Neer")
public class Brain extends TaskScript implements RenderListener {
    private final Task[] tasks = {new Quit(), new doBank(), new Exchange(), new Walk()};
    public static Position exChangePos = new Position(3164,3486,0);
    public static int bankCount = 0;
    public static int RealinvCount = 0;
    public static int EmptyOffers = 0;
    private static int Member = Game.getRemainingMembershipDays();
    public static boolean isMember = false;
    public static boolean GotBankStats = false;

    @Override
    public void onStart() {
        submit(tasks);
        Log.fine("Engage Bank Empty.");
    }

    @Override
    public void onStop(){
    }


    private static void DrawString(Graphics graphics, String string, int x, int y, Color color, Color shadow) {
        if (shadow != null) {
            graphics.setColor(shadow);
            graphics.drawString(string, x + 1, y - 1);
            graphics.drawString(string, x - 1, y - 1);
            graphics.drawString(string, x - 1, y);
            graphics.drawString(string, x + 1, y + 1);
            graphics.drawString(string, x - 1, y + 1);
            graphics.drawString(string, x + 1, y);
        }
        if (color != null) {
            graphics.setColor(color);
            graphics.drawString(string, x, y);
        }
        graphics.setColor(Color.WHITE);
    }

    private static void DrawString(Graphics graphics, String string, int x, int y) {
        DrawString(graphics, string, x, y, Color.WHITE, Color.BLACK);
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        RealinvCount = Inventory.getCount(item->item.isExchangeable() && item.getId() != 995);

        if (Member > 0) {
            isMember = true;
        }

        Graphics2D g = (Graphics2D) renderEvent.getSource();
        int x = 5;
        int y = 20;
        int alpha = 120; // Transparency
        Color BGColor = new Color(0, 0, 0, alpha);
        g.setFont(new Font("Arial", Font.BOLD, 15));
        g.setColor(BGColor);
        g.fillRect(10, 25, 125, 75);
        g.setColor(Color.white);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        DrawString(g,"[Bank Empty]",x += 10, y += 20); // BEGIN PAINT :)
        g.setFont(new Font("Arial", Font.BOLD, 10));
        DrawString(g,"Bank count: " + bankCount, x, y += 13);
        DrawString(g,"Inventory count: " + RealinvCount, x, y += 13);
        DrawString(g,"Empty offers: " + EmptyOffers, x, y += 13);
        DrawString(g, "Membership left: " + Member, x, y + 13);
    }
}
