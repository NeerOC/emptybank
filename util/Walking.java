package util;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

public class Walking {

    private static final int POSITION_RANDOMIZATION_OFFSET = 2;

    public static void walkTo(Position destination, int sufficientDistance, boolean randomized){

        if(sufficientDistance < getRemainingDistance(destination)){

            if(randomized){
                Movement.walkTo(randomizePosition(destination, sufficientDistance));
            }else{
                Movement.walkTo(destination);
            }

            Time.sleep(800);

            if (shouldActivateSprint() && !Movement.isRunEnabled())
                Movement.toggleRun(true);

            if(Movement.getDestination() != null && Movement.getDestination().distance(destination) <= sufficientDistance) {
                Time.sleepUntil(() -> Players.getLocal().getPosition().distance(destination) <= sufficientDistance || !Players.getLocal().isMoving(), Random.low(8000, 13000));
            }else{
                Time.sleepUntil(() -> Movement.getDestinationDistance() <= getMinimumDistance() || !Players.getLocal().isMoving(), Random.low(8000, 13000));
            }
        }
    }

    private static double getRemainingDistance(Position destination){

        if(Movement.getDestination() != null){
            return Movement.getDestination().distance(destination);
        }
        else {
            return Players.getLocal().getPosition().distance(destination);
        }

    }

    private static Position randomizePosition(Position position, int sufficientDistance) {

        if (sufficientDistance < POSITION_RANDOMIZATION_OFFSET)
            return position.randomize(sufficientDistance - 1);

        return position.randomize(POSITION_RANDOMIZATION_OFFSET);
    }

    private static boolean shouldActivateSprint() {
        return Movement.getRunEnergy() > 35 + Random.high(0, 12);
    }

    private static int getMinimumDistance(){
        return 5 + Random.nextInt(0, 4);
    }
}